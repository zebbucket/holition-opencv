//
//  OpenCVWrapper.m
//  HolitionOpenCVAppiOS
//
//  Created by Yousef El-Zebdeh on 06/07/2018.
//  Copyright © 2018 Yousef El-Zebdeh. All rights reserved.
//


#ifdef __cplusplus
#import "OpenCVWrapper.h"

#import <opencv2/opencv.hpp>
#import <opencv2/stitching/detail/blenders.hpp>
#import <opencv2/stitching/detail/exposure_compensate.hpp>


#import "opencv2/imgcodecs/ios.h"
#import <opencv2/stitching/detail/blenders.hpp>
#import <opencv2/stitching/detail/exposure_compensate.hpp>




@implementation OpenCVWrapper
+(NSString *) openCVVersionString
{
    return [NSString stringWithFormat:@"OpenCV Version %s", CV_VERSION];
}

+(UIImage *) makeGrayFromImage: (UIImage *)image
{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);

    if (imageMat.channels() == 1) return image;
    cv::Mat greyMat;
    cv::cvtColor(imageMat, greyMat, CV_BGR2GRAY);

    return MatToUIImage(greyMat);
}

@end

#endif
