//
//  OpenCVWrapper.h
//  HolitionOpenCVAppiOS
//
//  Created by Yousef El-Zebdeh on 06/07/2018.
//  Copyright © 2018 Yousef El-Zebdeh. All rights reserved.
//

#ifdef __cplusplus

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OpenCVWrapper : NSObject
    // returns OpenCV Version
+(NSString *) openCVVersionString;

    // converts an image to grayscale
+(UIImage *) makeGrayFromImage: (UIImage *)image ;
    
@end

#endif
