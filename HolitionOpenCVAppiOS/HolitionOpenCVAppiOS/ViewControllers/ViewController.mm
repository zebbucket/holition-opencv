//
//  ViewController.m
//  HolitionOpenCVAppiOS
//
//  Created by Yousef El-Zebdeh on 11/07/2018.
//  Copyright © 2018 Yousef El-Zebdeh. All rights reserved.
//

#import "ViewController.h"
#ifdef __cplusplus
#import "OpenCVWrapper.h"

#import <opencv2/opencv.hpp>
#import <opencv2/stitching/detail/blenders.hpp>
#import <opencv2/stitching/detail/exposure_compensate.hpp>


#import "opencv2/imgcodecs/ios.h"
#import <opencv2/stitching/detail/blenders.hpp>
#import <opencv2/stitching/detail/exposure_compensate.hpp>

#import "OpenCVWrapper.h"
#import "VideoSources.h"
#import "FrameProcessor.h"

#import <PureLayout/PureLayout.h>
#endif

@interface LeftFrameProcessor : NSObject<VideoSourceDelegate>

- (void)processFrame:(cv::Mat &)frame;

@end

@implementation LeftFrameProcessor
- (void)processFrame:(cv::Mat &)frame {
    // does nothing with the input
}
@end


@interface RightFrameProcessor : NSObject<VideoSourceDelegate>
- (void)processFrame:(cv::Mat &)frame;
@end

@implementation RightFrameProcessor
- (void)processFrame:(cv::Mat &)frame {
    cv::cvtColor(frame, frame, cv::COLOR_RGB2BGR);
}
@end




@interface ViewController ()
    @property (weak, nonatomic) IBOutlet UIView *leftView;
    @property (weak, nonatomic) IBOutlet UIView *rightView;
    @property(nonatomic, strong) VideoSource *videoSource;
    @property(nonatomic, strong) FrameProcessor *frameProcessor;
    @property(nonatomic, strong) LeftFrameProcessor *lFrameProcessor;
    @property(nonatomic, strong) RightFrameProcessor *rFrameProcessor;
@end

@implementation ViewController

- (instancetype)init
{
    self = [super init];
    if (self) { }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _lFrameProcessor = [[LeftFrameProcessor alloc] init];
    _rFrameProcessor = [[RightFrameProcessor alloc] init];
    _videoSource = [[VideoSource alloc] init];
//    _videoSource.delegate = _frameProcessor;

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
//    [self.videoSource setTargetView: _leftView];
//    self.videoSource.targetView = _leftView;
//
    [_videoSource addView:_lFrameProcessor :_leftView];
    [_videoSource addView:_lFrameProcessor :_rightView];
    
    [self.videoSource start ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
