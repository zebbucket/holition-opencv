#include <jni.h>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>


#include <android/log.h>

using namespace cv;

extern "C"
JNIEXPORT jstring

JNICALL
Java_com_example_yousef_holitionopencvappandroid_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}


extern "C"
{
void JNICALL Java_com_example_yousef_holitionopencvappandroid_MainActivity_salt(JNIEnv *env, jobject instance,
                                                                           jlong matAddrGray,
                                                                           jint nbrElem) {
    Mat &mGr = *(Mat *) matAddrGray;
    for (int k = 0; k < nbrElem; k++) {
        int i = rand() % mGr.cols;
        int j = rand() % mGr.rows;
        mGr.at<uchar>(j, i) = 255;
    }
}

void JNICALL Java_com_example_yousef_holitionopencvappandroid_MainActivity_rgbtobgr(JNIEnv *env,
                                                                                    jobject instance,
                                                                                    jlong matAddr) {
    Mat &mat = *(Mat *) matAddr;

//    static int count = 0;
//    if (count == 0){
//        __android_log_print(ANDROID_LOG_VERBOSE, "HolitionAndroid", "type %s  depth %s", mat.type(), mat.depth());
//        count++;
//    }

    cv::cvtColor(mat, mat, cv::COLOR_RGB2BGR);
}



}
