package com.example.yousef.holitionopencvappandroid

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.SurfaceView
import android.view.WindowManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

import org.opencv.*
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.CameraBridgeViewBase
import org.opencv.android.OpenCVLoader
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame
import org.opencv.android.LoaderCallbackInterface
import org.opencv.core.Mat
import org.opencv.core.CvType
import org.opencv.imgproc.Imgproc


class MainActivity : Activity(), CameraBridgeViewBase.CvCameraViewListener2 {

    init
    {
        System.loadLibrary("native-lib");
    }

    external fun stringFromJNI(): String

    external fun salt(matAddrGray: Long, nbrElem: Int)
    external fun rgbtobgr(matAddrGray: Long)


    private var mOpenCvCameraView: CameraBridgeViewBase? = null
    private var mIsJavaCamera = true
    private var mItemSwitchCamera: MenuItem? = null

    private val mLoaderCallback = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS -> {
                    Log.i(TAG, "OpenCV loaded successfully")
                    mOpenCvCameraView!!.enableView()
                }
                else -> {
                    super.onManagerConnected(status)
                }
            }
        }
    }

    init {
        Log.i(TAG, "Instantiated new " + this.javaClass)
    }

    /** Called when the activity is first created.  */
    public override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(TAG, "called onCreate")
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        setContentView(R.layout.activity_main)

        if (mIsJavaCamera)
            mOpenCvCameraView = findViewById(R.id.java_camera_view) as CameraBridgeViewBase
        else
            mOpenCvCameraView = findViewById(R.id.native_camera_view) as CameraBridgeViewBase

        mOpenCvCameraView!!.visibility = SurfaceView.VISIBLE

        mOpenCvCameraView!!.setCvCameraViewListener(this)
    }

    public override fun onPause() {
        super.onPause()
        if (mOpenCvCameraView != null)
            mOpenCvCameraView!!.disableView()
    }

    public override fun onResume() {
        super.onResume()
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback)
    }

    public override fun onDestroy() {
        super.onDestroy()
        if (mOpenCvCameraView != null)
            mOpenCvCameraView!!.disableView()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        Log.i(TAG, "called onCreateOptionsMenu")
        mItemSwitchCamera = menu.add("Toggle Native/Java camera")
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var toastMesage = String()
        Log.i(TAG, "called onOptionsItemSelected; selected item: $item")

        if (item === mItemSwitchCamera) {
            mOpenCvCameraView!!.visibility = SurfaceView.GONE
            mIsJavaCamera = !mIsJavaCamera

            if (mIsJavaCamera) {
                mOpenCvCameraView = findViewById(R.id.java_camera_view) as CameraBridgeViewBase
                toastMesage = "Java Camera"
            } else {
                mOpenCvCameraView = findViewById(R.id.native_camera_view) as CameraBridgeViewBase
                toastMesage = "Native Camera"
            }

            mOpenCvCameraView!!.visibility = SurfaceView.VISIBLE
            mOpenCvCameraView!!.setCvCameraViewListener(this)
            mOpenCvCameraView!!.enableView()
            val toast = Toast.makeText(this, toastMesage, Toast.LENGTH_LONG)
            toast.show()
        }

        return true
    }

    private var intermediate: Mat? = null

    override fun onCameraViewStarted(width: Int, height: Int) {
        intermediate = Mat(height, width, CvType.CV_8UC4)

    }

    override fun onCameraViewStopped() {
        intermediate?.release()
    }

    override fun onCameraFrame(inputFrame: CvCameraViewFrame): Mat? {

        intermediate = inputFrame.rgba()

        var output = intermediate
        if (output == null) {
            return inputFrame.rgba()
        } else{
            rgbtobgr(output.nativeObjAddr)
            return output
        }

    }

    companion object {
        private val TAG = "OCVSample::Activity"
    }
}
